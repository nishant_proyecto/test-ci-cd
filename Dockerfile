FROM node:10

COPY dist /usr/src/app/dist

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./
RUN ls /usr/src/app/dist
RUN npm install
# RUN npm ci
# If you are building your code for production
#RUN npm ci --only=production

ENV PORT 3000
EXPOSE 3000
CMD [ "node", "/usr/src/app/dist/index.js"]}